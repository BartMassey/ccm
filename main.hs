--- Copyright © 2008 Bart Massey
--- ALL RIGHTS RESERVED
--- [This program is licensed under the "3-clause ('new') BSD License"]
--- Please see the file COPYING in the source
--- distribution of this software for license terms.

import System.Environment
import CCM
import Data.List
import Data.Bits

combine = foldl' xor 0

concatMap_openinline :: (a -> [b]) -> [a] -> [b]
concatMap_openinline f l = cmap l where
    cmap [] = []
    cmap (x : xs) = accum (f x) where
        accum [] = cmap xs
        accum (y : ys) = y : accum ys

main = do
  [argc, argk, argn] <- getArgs
  let k = read argk
  let n = read argn
  case argc of
    "none" ->
        putStrLn . show $ (combine . map combine . makeList k) n
    "std" ->
        putStrLn . show $ (combine . concatMap_std myId . makeList k) n
    "open" ->
        putStrLn . show $ (combine . concatMap_open myId . makeList k) n
    "obvious" ->
        putStrLn . show $ (combine . concatMap_obvious myId . makeList k) n
    "openinline" ->
        putStrLn . show $ (combine . concatMap_openinline myId . makeList k) n
    "openinlineall" ->
        putStrLn . show $ (combine . concatMap_openinline id . makeList k) n
