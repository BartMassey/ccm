--- Copyright © 2008 Bart Massey
--- ALL RIGHTS RESERVED
--- [This program is licensed under the "3-clause ('new') BSD License"]
--- Please see the file COPYING in the source
--- distribution of this software for license terms.

module CCM
where

import Data.List

concatMap_std :: (a -> [b]) -> [a] -> [b]
concatMap_std f =  foldr ((++) . f) []

concatMap_open :: (a -> [b]) -> [a] -> [b]
concatMap_open f l = cmap l where
    cmap [] = []
    cmap (x : xs) = accum (f x) where
        accum [] = cmap xs
        accum (y : ys) = y : accum ys
    
concatMap_obvious :: (a -> [b]) -> [a] -> [b]
concatMap_obvious f = concat . map f

myId :: a -> a
myId x = x

makeList :: Int -> Int -> [[Int]]
makeList k n = segments 1 where
    segments i | i <= n - k = [i..(i + k - 1)] : segments (i + k)
    segments i | i <= n = [[i..n]]
