#!/bin/sh
cat <<EOF |
none
std
open
obvious
openinline
openinlineall
EOF
while read ALG
do
    echo $ALG
    sh timing.sh $ALG
done
