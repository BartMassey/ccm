#!/bin/sh
# Copyright © 2008 Bart Massey
# ALL RIGHTS RESERVED
# [This program is licensed under the "3-clause ('new') BSD License"]
# Please see the file COPYING in the source
# distribution of this software for license terms.
( echo 'set terminal png'
  echo 'set output "times.png"'
  echo 'set logscale x'
  echo -n 'plot '
  for i in *.dat
  do
    echo -n "'$i'"
  done | sed "s=''=','=g"
  echo "" ) | gnuplot
