#!/bin/sh
# Copyright © 2008 Bart Massey
# ALL RIGHTS RESERVED
# [This program is licensed under the "3-clause ('new') BSD License"]
# Please see the file COPYING in the source
# distribution of this software for license terms.
PGM="`basename $0`"
NICKLE="`/bin/which nickle`"
if [ "$NICKLE" = "" ]
then
    echo "$PGM: you need Nickle (http://nickle.org) to run this" >&2
    exit 1
fi
USAGE="$PGM: usage: $PGM <alg> [<base>]"
if [ ! -x ./main ] ; then echo "no executable" >&2; exit 1; fi
BASE=10
LOGN=7
ALG=$1; shift
if [ $# -ne 0 ] ; then LOGN=$1; shift; fi
LAST=`nickle -e "$LOGN*$BASE"`
N=`nickle -e "$BASE**$LOGN"`

# XXX this horrible workaround is because
# GNU time is POSIX-incompatible by default.
TIMEFMT=`cat <<'EOF'
real %e
user %U
sys %S
EOF`

for I in `seq $BASE $LAST`
do
  K=`nickle -e "floor($BASE**($I/$BASE)+0.5)"`
  # echo ./main $ALG $K $N
  ( TIME="$TIMEFMT" time ./main $ALG $K $N ) 2>&1 |
    awk "/real/{print $K, \$2;}" |
    sed 's/[0-9]m\([0-9.]*\)s/\1/'
done > times-$ALG.dat
